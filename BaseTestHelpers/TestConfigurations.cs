﻿
namespace BaseTestHelpers
{
    /// <summary>
    /// Holds configurations like you would in a JSON file since connection strings are to local Databases, there is not sensitive data
    /// This class should only hold configurations that are shared across between different test projects
    /// </summary>
    public static class TestConfigurations
    {
        public static string JotDb =
            "Data Source = localhost\\SQLEXPRESS;Initial Catalog = JotCore; Integrated Security = True; Connect Timeout = 30; Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public static string OtisDb =
            "Data Source = localhost\\SQLEXPRESS;Initial Catalog = OtisCore; Integrated Security = True; Connect Timeout = 30; Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
    }
}
