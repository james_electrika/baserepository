﻿namespace BaseTestHelpers.Interfaces
{
    public interface IServerFixture : IBaseServerFixture
    {
        void Authenticate();
        void DeAuthenticate();
    }
}
