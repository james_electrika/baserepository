﻿using System.Net.Http;

namespace BaseTestHelpers.Interfaces
{
    public interface IBaseServerFixture
    {
        HttpClient Client { get; }
    }
}
