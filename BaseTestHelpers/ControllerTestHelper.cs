﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BaseRepository.Models;
using BaseTestHelpers.Interfaces;
using Newtonsoft.Json;
using Xunit;

namespace BaseTestHelpers
{
    /// <summary>
    /// This class serves a base to make a controller test base to test api end points and controllers
    /// Requests are executed and assertions are made by the class on the http result status codes
    /// Is just missing the IServerFixture dependency that can be injected by Xunit, just add the following to your class declaration
    ///  : <see cref="BaseControllerTester" />, <see cref="IClassFixture{TServerFixture}"/> where <c>TServerFixture</c> is the implementation of test server fixture for your test project
    /// </summary>
    public abstract class BaseControllerTester
    {
        /// <summary>
        /// Test server object, contains the http client to make requests to the api under test
        /// </summary>
        protected readonly IServerFixture Server;

        protected BaseControllerTester(IServerFixture server)
        {
            Server = server;
        }

        /// <summary>
        /// Execute a Get Request to the passed endpoint in the query string and asserts against the result http status code
        /// </summary>
        /// <param name="queryString">Url for the request</param>
        protected async Task GetReturnsUnauthorized(string queryString)
        {
            Server.DeAuthenticate();

            using (var response = await Server.Client.GetAsync(queryString))
            {
                Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
            }

            //cleanup
            Server.Authenticate();
        }

        /// <summary>
        /// Execute a Get Request to the passed endpoint in the query string and asserts against the result http status code
        /// </summary>
        /// <param name="queryString">Url for the request</param>

        protected async Task GetReturnsForbidden(string queryString)
        {
            using (var response = await Server.Client.GetAsync(queryString))
            {
                Assert.Equal(HttpStatusCode.Forbidden, response.StatusCode);
            }
        }

        /// <summary>
        /// Execute a Get Request to the passed endpoint in the query string and asserts against the result http status code
        /// </summary>
        /// <param name="queryString">Url for the request</param>
        protected async Task GetReturnsOk(string queryString)
        {
            using (var response = await Server.Client.GetAsync(queryString))
            {
                Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            }
        }

        /// <summary>
        /// Execute a Get Request to the passed endpoint in the query string and asserts against the result http status code
        /// </summary>
        /// <param name="queryString">Url for the request</param>
        protected async Task GetReturnsNoContent(string queryString)
        {
            using (var response = await Server.Client.GetAsync(queryString))
            {
                Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
            }
        }

        /// <summary>
        /// Execute a Get Request to the passed endpoint in the query string and asserts against the result http status code
        /// It also checks if the result contains a non empty list of the specified type
        /// </summary>
        /// <typeparam name="T">Type of objects in the result list</typeparam>
        /// <param name="queryString">Url for the request</param>
        protected async Task GetReturnsListOfType<T>(string queryString)
        {
            using (var response = await Server.Client.GetAsync(queryString))
            {
                Assert.Equal(HttpStatusCode.OK, response.StatusCode);

                var content = await response.Content.ReadAsStringAsync();
                var list = JsonConvert.DeserializeObject(content, typeof(List<T>)) as List<T>;

                Assert.NotNull(list);
                Assert.NotEmpty(list);
            }
        }

        /// <summary>
        /// Execute a Get Request to the passed endpoint in the query string and asserts against the result http status code
        /// It also checks if the result contains a non empty list of the specified type
        /// </summary>
        /// <typeparam name="T">Type of objects in the result list</typeparam>
        /// <param name="queryString">Url for the request</param>
        protected async Task GetReturnsPagedResultOfType<T>(string queryString)
        {
            using (var response = await Server.Client.GetAsync(queryString))
            {
                Assert.Equal(HttpStatusCode.OK, response.StatusCode);

                var content = await response.Content.ReadAsStringAsync();
                var list = JsonConvert.DeserializeObject(content, typeof(PagedResult<T>)) as PagedResult<T>;

                Assert.NotNull(list);
                Assert.NotEmpty(list.Results);
            }
        }

        /// <summary>
        /// Execute a Get Request to the passed endpoint in the query string and asserts against the result http status code
        /// It also checks if the result is none null and that it has the supplied amount of elements
        /// </summary>
        /// <typeparam name="T">Type of objects in the result list</typeparam>
        /// <param name="queryString">Url for the request</param>
        /// <param name="count">Amount of elements to check for in list</param>
        protected async Task GetReturnsListWithCount<T>(string queryString, int count)
        {
            using (var response = await Server.Client.GetAsync(queryString))
            {
                Assert.Equal(HttpStatusCode.OK, response.StatusCode);

                var content = await response.Content.ReadAsStringAsync();
                var list = JsonConvert.DeserializeObject(content, typeof(List<T>)) as List<T>;

                Assert.NotNull(list);
                Assert.Equal(count, list.Count);
            }
        }

        /// <summary>
        /// Execute a Get Request to the passed endpoint in the query string and asserts against the result http status code
        /// It also checks if the result is none null and that it has the supplied amount of elements
        /// </summary>
        /// <typeparam name="T">Type of objects in the result list</typeparam>
        /// <param name="queryString">Url for the request</param>
        /// <param name="count">Amount of elements to check for in list</param>
        protected async Task GetReturnsPagedResultWithCount<T>(string queryString, int count)
        {
            using (var response = await Server.Client.GetAsync(queryString))
            {
                Assert.Equal(HttpStatusCode.OK, response.StatusCode);

                var content = await response.Content.ReadAsStringAsync();
                var list = JsonConvert.DeserializeObject(content, typeof(PagedResult<T>)) as PagedResult<T>;

                Assert.NotNull(list);
                Assert.Equal(count, list.Results.Count);
            }
        }

        /// <summary>
        /// Execute a Get Request to the passed endpoint in the query string,
        /// verifies that the object of specified type is valid and returns the object for further assertions
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryString"></param>
        protected async Task<T> GetReturnsType<T>(string queryString)
        {
            using (var response = await Server.Client.GetAsync(queryString))
            {
                var content = await response.Content.ReadAsStringAsync();

                var obj = JsonConvert.DeserializeObject(content, typeof(T));

                Assert.NotNull(obj);
                return (T)obj;
            }
        }

        /// <summary>
        /// Execute a Post request to the specified endpoint, passes a request body and asserts against the result http status code
        /// </summary>
        /// <param name="requestObject">Request body</param>
        /// <param name="queryString">Url to endpoint</param>
        protected async Task PostReturnsUnauthorized(object requestObject, string queryString)
        {
            Server.DeAuthenticate();

            using (var response = await ExecutePost(requestObject, queryString))
            {
                Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
            }

            //cleanup
            Server.Authenticate();
        }

        /// <summary>
        /// Execute a Post request to the specified endpoint, passes a request body and asserts against the result http status code
        /// </summary>
        /// <param name="requestObject">Request body</param>
        /// <param name="queryString">Url to endpoint</param>
        protected async Task PostReturnsForbidden(object requestObject, string queryString)
        {
            using (var response = await ExecutePost(requestObject, queryString))
            {
                Assert.Equal(HttpStatusCode.Forbidden, response.StatusCode);
            }
        }

        /// <summary>
        /// Execute a Post request to the specified endpoint, passes a request body and asserts against the result http status code
        /// </summary>
        /// <param name="requestObject">Request body</param>
        /// <param name="queryString">Url to endpoint</param>
        protected async Task PostReturnsNoContent(object requestObject, string queryString)
        {
            using (var response = await ExecutePost(requestObject, queryString))
            {
                Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
            }
        }

        /// <summary>
        /// Execute a Post request to the specified endpoint, passes a request body and asserts against the result http status code
        /// </summary>
        /// <param name="requestObject">Request body</param>
        /// <param name="queryString">Url to endpoint</param>
        protected async Task PostReturnsMethodNotAllowed(object requestObject, string queryString)
        {
            using (var response = await ExecutePost(requestObject, queryString))
            {
                Assert.Equal(HttpStatusCode.MethodNotAllowed, response.StatusCode);
            }
        }

        /// <summary>
        /// Execute a Post request to the specified endpoint, passes a request body and asserts against the result http status code
        /// </summary>
        /// <param name="requestObject">Request body</param>
        /// <param name="queryString">Url to endpoint</param>
        protected async Task PostReturnsBadRequest(object requestObject, string queryString)
        {
            using (var response = await ExecutePost(requestObject, queryString))
            {
                Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        /// <summary>
        /// Execute a Post request to the specified endpoint, passes a request body and asserts against the result http status code
        /// </summary>
        /// <param name="requestObject">Request body</param>
        /// <param name="queryString">Url to endpoint</param>
        protected async Task PostReturnsOk(object requestObject, string queryString)
        {
            using (var response = await ExecutePost(requestObject, queryString))
            {
                Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            }
        }

        /// <summary>
        /// Execute a Post request to the specified endpoint, passes a request body and asserts against the result http status code
        /// </summary>
        /// <param name="requestObject">Request body</param>
        /// <param name="queryString">Url to endpoint</param>
        protected async Task PostReturnsCreated(object requestObject, string queryString)
        {
            using (var response = await ExecutePost(requestObject, queryString))
            {
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            }
        }

        /// <summary>
        /// Execute a Post request to the specified endpoint, passes a request body and asserts against the result http status code
        /// It also checks if the result contains a non empty list of the specified type
        /// </summary>
        /// <param name="requestObject">Request body</param>
        /// <param name="queryString">Url to endpoint</param>
        protected async Task PostReturnsLisOfType<T>(object requestObject, string queryString)
        {
            using (var response = await ExecutePost(requestObject, queryString))
            {
                Assert.Equal(HttpStatusCode.OK, response.StatusCode);

                var content = await response.Content.ReadAsStringAsync();
                var list = JsonConvert.DeserializeObject(content, typeof(List<T>)) as List<T>;

                Assert.NotNull(list);
                Assert.NotEmpty(list);
            }
        }

        /// <summary>
        /// Execute a Delete request to the specified endpoint, passes a request body and asserts against the result http status code
        /// </summary>
        /// <param name="queryString">Url to endpoint</param>
        protected async Task DeleteReturnsUnauthorized(string queryString)
        {
            Server.DeAuthenticate();

            using (var response = await ExecuteDelete(queryString))
            {
                Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
            }

            //cleanup
            Server.Authenticate();
        }

        /// <summary>
        /// Execute a Delete request to the specified endpoint, passes a request body and asserts against the result http status code
        /// </summary>
        /// <param name="queryString">Url to endpoint</param>
        protected async Task DeleteReturnsMethodNotAllowed(string queryString)
        {
            using (var response = await ExecuteDelete(queryString))
            {
                Assert.Equal(HttpStatusCode.MethodNotAllowed, response.StatusCode);
            }
        }

        /// <summary>
        /// Execute a Delete request to the specified endpoint, passes a request body and asserts against the result http status code
        /// </summary>
        /// <param name="queryString">Url to endpoint</param>
        protected async Task DeleteReturnsNoContent(string queryString)
        {
            using (var response = await ExecuteDelete(queryString))
            {
                Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
            }
        }

        /// <summary>
        /// Execute a Post request to the passed endpoint in the query string,
        /// passes the request body and verifies that the object of specified type is valid and returns the object for further assertions
        /// </summary>
        /// <param name="requestObject">Request body</param>
        /// <param name="queryString">Url to endpoint</param>
        protected async Task<T> PostReturnsType<T>(object requestObject, string queryString)
        {
            using (var response = await ExecutePost(requestObject, queryString))
            {
                var content = await response.Content.ReadAsStringAsync();
                var obj = JsonConvert.DeserializeObject(content, typeof(T));

                Assert.NotNull(obj);
                return (T)obj;
            }
        }

        /// <summary>
        /// Execute a Put request to the specified endpoint, passes a request body and asserts against the result http status code
        /// </summary>
        /// <param name="requestObject">Request body</param>
        /// <param name="queryString">Url to endpoint</param>
        protected async Task PutReturnsUnauthorized(object requestObject, string queryString)
        {
            Server.DeAuthenticate();

            using (var response = await ExecutePut(requestObject, queryString))
            {
                Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
            }

            //cleanup
            Server.Authenticate();
        }

        /// <summary>
        /// Execute a Put request to the specified endpoint, passes a request body and asserts against the result http status code
        /// </summary>
        /// <param name="requestObject">Request body</param>
        /// <param name="queryString">Url to endpoint</param>

        protected async Task PutReturnsNoContent(object requestObject, string queryString)
        {
            using (var response = await ExecutePut(requestObject, queryString))
            {
                Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
            }
        }

        /// <summary>
        /// Execute a Post request to the specified endpoint, passes a request body and asserts against the result http status code
        /// </summary>
        /// <param name="requestObject">Request body</param>
        /// <param name="queryString">Url to endpoint</param>
        protected async Task PutReturnsForbidden(object requestObject, string queryString)
        {
            using (var response = await ExecutePut(requestObject, queryString))
            {
                Assert.Equal(HttpStatusCode.Forbidden, response.StatusCode);
            }
        }

        /// <summary>
        /// Execute a Put request to the specified endpoint, passes a request body and asserts against the result http status code
        /// </summary>
        /// <param name="requestObject">Request body</param>
        /// <param name="queryString">Url to endpoint</param>
        protected async Task PutReturnsOk(object requestObject, string queryString)
        {
            using (var response = await ExecutePut(requestObject, queryString))
            {
                Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            }
        }

        /// <summary>
        /// Execute a Put request to the passed endpoint in the query string,
        /// passes the request body and verifies that the object of specified type is valid and returns the object for further assertions
        /// </summary>
        /// <param name="requestObject">Request body</param>
        /// <param name="queryString">Url to endpoint</param>
        protected async Task<T> PutReturnsType<T>(object requestObject, string queryString)
        {
            using (var response = await ExecutePut(requestObject, queryString))
            {
                var content = await response.Content.ReadAsStringAsync();
                var obj = JsonConvert.DeserializeObject(content, typeof(T));

                Assert.NotNull(obj);
                return (T)obj;
            }
        }

        private async Task<HttpResponseMessage> ExecutePost(object requestObject, string queryString)
        {
            var json = JsonConvert.SerializeObject(requestObject);
            var j = new StringContent(json, Encoding.UTF8, "application/json");

            return await Server.Client.PostAsync(queryString, j);
        }

        private async Task<HttpResponseMessage> ExecutePut(object requestObject, string queryString)
        {
            var json = JsonConvert.SerializeObject(requestObject);
            var j = new StringContent(json, Encoding.UTF8, "application/json");

            return await Server.Client.PutAsync(queryString, j);
        }

        private async Task<HttpResponseMessage> ExecuteDelete(string queryString)
        {
            return await Server.Client.DeleteAsync(queryString);
        }
    }
}
