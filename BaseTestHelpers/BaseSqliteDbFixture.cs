﻿using System;
using Microsoft.EntityFrameworkCore;

namespace BaseTestHelpers
{
    /// <summary>
    /// Base class for an sqlite database context
    /// This is used to test entity framework or simple dapper queries
    /// In some cases you will probably need to create a variant of your original db context to remove provider specific configurations (like varchar(max))
    /// In those cases the TContext would be variant and TOptionsContext would be the original DbContext
    /// </summary>
    /// <typeparam name="TContext">DbContext type from your EF project</typeparam>
    /// <typeparam name="TOptionsContext">DbContext to get the builder options from</typeparam>
    public abstract class BaseSqliteDbFixture<TContext, TOptionsContext> : IDisposable where TContext : DbContext where TOptionsContext : DbContext
    {
        public TContext Context;

        protected BaseSqliteDbFixture()
        {
            var builder = GetBuilder();
            builder.UseSqlite("DataSource=:memory:", _ => { }).EnableSensitiveDataLogging();
            Context = GetContext(builder.Options);
            Context.Database.OpenConnection();
            Context.Database.EnsureCreated();
        }

        /// <summary>
        /// Removes the test database from memory
        /// This method should be called automatically by the test framework but you can trigger it manually
        /// </summary>
        public void Dispose()
        {
            Context.Database.EnsureDeleted();
            Context.Dispose();
        }

        /// <summary>
        /// Since using generics the context can't be properly instantiated, implement this function to instantiate your context
        /// Ex: return new TContext(options)
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        protected abstract TContext GetContext(DbContextOptions<TOptionsContext> options);

        protected DbContextOptionsBuilder<TOptionsContext> GetBuilder()
        {
            return new DbContextOptionsBuilder<TOptionsContext>();
        }
    }
}
