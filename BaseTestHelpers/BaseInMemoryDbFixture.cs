﻿using System;
using Microsoft.EntityFrameworkCore;

namespace BaseTestHelpers
{
    /// <summary>
    /// Base for an in memory database context
    /// This is used to test entity framework operations
    /// </summary>
    /// <typeparam name="TContext">Database context type from your EF implementation</typeparam>
    public abstract class BaseInMemoryDbFixture<TContext> : IDisposable where TContext : DbContext
    {
        /// <summary>
        /// Test DbContext
        /// </summary>
        public readonly TContext Context;

        protected BaseInMemoryDbFixture()
        {
            var builder = GetBuilder();
            builder.UseInMemoryDatabase(Guid.NewGuid().ToString()).EnableSensitiveDataLogging();
            Context = GetContext(builder.Options);
            Context.Database.EnsureCreated();
        }

        /// <summary>
        /// Removes the test database from memory
        /// This method should be called automatically by the test framework but you can trigger it manually
        /// </summary>
        public void Dispose()
        {
            Context.Database.EnsureDeleted();
            Context.Dispose();
        }

        /// <summary>
        /// Since using generics the context can't be properly instantiated, implement this function to instantiate your context
        /// Ex: return new TContext(options)
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        protected abstract TContext GetContext(DbContextOptions<TContext> options);

        private static DbContextOptionsBuilder<TContext> GetBuilder()
        {
            return new DbContextOptionsBuilder<TContext>();
        }
    }
}
