﻿using System;
using System.Net.Http;
using BaseTestHelpers.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;

namespace BaseTestHelpers
{
    /// <summary>
    /// Base class for a .net core test server implementation
    /// This class is used for integration test of api end points and controllers. Takes the startup file from your .net core project and starts a test server
    /// 
    /// </summary>
    /// <typeparam name="TStartup"></typeparam>
    public abstract class BaseTestServerFixture<TStartup> : IDisposable, IServerFixture where TStartup : class
    {
        /// <summary>
        /// HttpClient, used to make request from your test class to your test server
        /// </summary>
        public HttpClient Client { get; }
        private readonly TestServer _testServer;

        protected BaseTestServerFixture(string settingsFile, string environment)
        {
            //configuration files get copied to the output folder
            var projectDir = System.IO.Directory.GetCurrentDirectory();
            var configBuilder = new ConfigurationBuilder().SetBasePath(projectDir).AddJsonFile(settingsFile).Build();
            var builder = new WebHostBuilder().UseEnvironment(environment).UseConfiguration(configBuilder).UseStartup<TStartup>();
            ConfigureBuilder(builder);
            _testServer = new TestServer(builder);
            Client = _testServer.CreateClient();
        }

        public void Dispose()
        {
            Client.Dispose();
            _testServer.Dispose();
        }

        /// <summary>
        /// Optional Method, use it if you need to add any extra configurations to your web host builder
        /// </summary>
        /// <param name="builder"></param>

        public virtual void ConfigureBuilder(IWebHostBuilder builder)
        {
        }

        /// <summary>
        /// Authentication logic, needed to pass tests on authorized methods
        /// </summary>
        public abstract void Authenticate();

        /// <summary>
        /// De authentication logic, used to test behaviour on non authenticated users. Is need if you are not authenticated by default
        /// </summary>
        public abstract void DeAuthenticate();
    }
}
