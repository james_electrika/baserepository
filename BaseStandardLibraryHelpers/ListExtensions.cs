﻿using System.Collections.Generic;

namespace BaseStandardLibraryHelpers
{
    public static class ListExtensions
    {
        public static bool CompareLists<T>(this List<T> aListA, List<T> aListB)
        {
            if (aListA == null || aListB == null || aListA.Count != aListB.Count)
                return false;

            if (aListA.Count == 0)
                return true;

            var lookUp = new Dictionary<T, int>();

            // create index for the first list
            foreach (var item in aListA)
            {
                if (!lookUp.TryGetValue(item, out var count))
                {
                    lookUp.Add(item, 1);
                    continue;
                }
                lookUp[item] = count + 1;
            }

            foreach (var item in aListB)
            {
                if (!lookUp.TryGetValue(item, out var count))
                {
                    // early exit as the current value in B doesn't exist in the lookUp (and not in ListA)
                    return false;
                }
                count--;
                if (count <= 0)
                    lookUp.Remove(item);
                else
                    lookUp[item] = count;
            }

            // if there are remaining elements in the lookUp, that means ListA contains elements that do not exist in ListB
            return lookUp.Count == 0;
        }
    }
}
