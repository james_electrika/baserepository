﻿using BaseRepository;
using BaseRepository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BaseRepositoryTests
{
    public class TestRepository : Repository<TestEntity, int>
    {
        public TestRepository(DbContext context, IDbConnectionFactory connectionFactory) : base(context,
            connectionFactory)
        {
        }
    }
}
