﻿using System;
using System.Linq;
using BaseRepository;
using BaseRepository.Interfaces;
using Moq;
using Xunit;

namespace BaseRepositoryTests
{
    public class UnitOfWorkTests
    {
        private readonly TestDbContext _context;
        private readonly IUnitOfWork _unitOfWork;

        public UnitOfWorkTests()
        {
            var dbFixture = new TestSqliteDbFixture();
            _context = dbFixture.Context;
            var dbConnectionFactory = new Mock<IDbConnectionFactory>();
            _unitOfWork = new UnitOfWork<TestDbContext, IDbConnectionFactory>(_context, dbConnectionFactory.Object);
        }

        [Fact]
        public async void Transaction_WhenExceptionRaises_ChangesAreRolledBack()
        {
            var preCheck = _context.TestEntities.FirstOrDefault(i => i.Value == "24");

            Assert.Null(preCheck);

            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                await _unitOfWork.Transaction(async () =>
                {
                    await _context.TestEntities.AddAsync(new TestEntity()
                    {
                        Value = "24"
                    });

                    await _context.SaveChangesAsync();

                    var inCheck = _context.TestEntities.FirstOrDefault(i => i.Value == "24");

                    Assert.NotNull(inCheck);

                    throw new ArgumentNullException();
                });
            });

            var postCheck = _context.TestEntities.FirstOrDefault(i => i.Value == "24");

            Assert.Null(postCheck);
        }
    }
}
