﻿using Microsoft.EntityFrameworkCore;

namespace BaseRepositoryTests
{
    public class TestDbContext : DbContext
    {
        public DbSet<TestEntity> TestEntities { get; set; }

        public TestDbContext(DbContextOptions options) : base(options)
        {
        }
    }
}
