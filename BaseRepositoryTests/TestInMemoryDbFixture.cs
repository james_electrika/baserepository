﻿using BaseTestHelpers;
using Microsoft.EntityFrameworkCore;

namespace BaseRepositoryTests
{
    public class TestInMemoryDbFixture : BaseInMemoryDbFixture<TestDbContext>
    {
        protected override TestDbContext GetContext(DbContextOptions<TestDbContext> options)
        {
            return new TestDbContext(options);
        }
    }
}
