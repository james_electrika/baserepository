﻿using BaseTestHelpers;
using Microsoft.EntityFrameworkCore;

namespace BaseRepositoryTests
{
    public class TestSqliteDbFixture : BaseSqliteDbFixture<TestDbContext, TestDbContext>
    {
        protected override TestDbContext GetContext(DbContextOptions<TestDbContext> options)
        {
            return new TestDbContext(options);
        }
    }
}
