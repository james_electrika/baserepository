using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BaseRepository.Interfaces;
using BaseRepository.Models;
using Moq;
using Xunit;

namespace BaseRepositoryTests
{
    public class RepositoryTests
    {
        private readonly TestDbContext _testContext;
        private readonly TestRepository _testRepository;

        public RepositoryTests()
        {
            var dbFixture = new TestInMemoryDbFixture();
            var mockConnectionFactory = new Mock<IDbConnectionFactory>();

            _testContext = dbFixture.Context;
            _testRepository = new TestRepository(_testContext, mockConnectionFactory.Object);
        }

        [Fact]
        public void GetAll_ReturnsNonEmptyResult()
        {
            SeedEntities();

            var result = _testRepository.GetAll();

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void GetAll_WhenEmptyResultSet_ReturnsEmptyResult()
        {
            var result = _testRepository.GetAll();

            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetAllAsync_ReturnsNonEmptyResult()
        {
            SeedEntities();

            var result = await _testRepository.GetAllAsync();

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public async Task GetAllAsync_WhenEmptyResultSet_ReturnsEmptyResult()
        {
            var result = await _testRepository.GetAllAsync();

            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public void Get_GivenValidInput_ReturnsValue()
        {
            SeedEntities();

            var result = _testRepository.Get(1);

            Assert.NotNull(result);
            Assert.Equal(1, result.Id);
        }

        [Fact]
        public void Get_GivenInValidId_ReturnsNull()
        {
            SeedEntities();

            var result = _testRepository.Get(10);

            Assert.Null(result);
        }

        [Fact]
        public async Task GetAsync_GivenValidInput_ReturnsValue()
        {
            SeedEntities();

            var result = await _testRepository.GetAsync(1);

            Assert.NotNull(result);
            Assert.Equal(1, result.Id);
        }

        [Fact]
        public async Task GetAsync_GivenInValidId_ReturnsNull()
        {
            SeedEntities();

            var result = await _testRepository.GetAsync(10);

            Assert.Null(result);
        }

        [Fact]
        public async Task GetPagedResultsAsync_GivenInValidPagination_ReturnsEmpty()
        {
            SeedEntities();

            var query = new PageQuery { PageSize = 2, Page = 10 };

            var result = await _testRepository.GetPagedResultsAsync(query);

            Assert.NotNull(result);
            Assert.Empty(result.Results);
        }

        [Fact]
        public async Task GetPagedResultsAsync_ReturnsCorrectRowCount()
        {
            SeedEntities();

            var query = new PageQuery { PageSize = 2, Page = 1 };

            var result = await _testRepository.GetPagedResultsAsync(query);

            Assert.Equal(4, result.RowCount);
        }

        [Fact]
        public async Task GetPagedResultsAsync_GivenValidPaginationValues_ReturnsPage()
        {
            SeedEntities();

            var query = new PageQuery { PageSize = 2, Page = 1 };

            var result = await _testRepository.GetPagedResultsAsync(query);

            Assert.NotNull(result);
            Assert.NotEmpty(result.Results);
            Assert.Equal(2, result.Results.Count);
            Assert.Equal(3, result.Results.FirstOrDefault()?.Id);
        }

        [Fact]
        public void GetPagedResults_ReturnsCorrectRowCount()
        {
            SeedEntities();

            var query = new PageQuery { PageSize = 2, Page = 1 };

            var result = _testRepository.GetPagedResults(query);

            Assert.Equal(4, result.RowCount);
        }

        [Fact]
        public void GetPagedResults_GivenInValidPagination_ReturnsEmpty()
        {
            SeedEntities();

            var query = new PageQuery { PageSize = 2, Page = 10 };

            var result = _testRepository.GetPagedResults(query);

            Assert.NotNull(result);
            Assert.Empty(result.Results);
        }

        [Fact]
        public void GetPagedResults_GivenValidPaginationValues_ReturnsPage()
        {
            SeedEntities();

            var query = new PageQuery { PageSize = 2, Page = 1 };

            var result = _testRepository.GetPagedResults(query);

            Assert.NotNull(result);
            Assert.NotEmpty(result.Results);
            Assert.Equal(2, result.Results.Count);
            Assert.Equal(3, result.Results.FirstOrDefault()?.Id);
        }

        [Fact]
        public async Task GetAllAsync_ReturnsAllEntities()
        {
            SeedEntities();

            var result = await _testRepository.GetAllAsync();

            Assert.NotNull(result);
            Assert.NotEmpty(result);
            Assert.Equal(4, result.Count());
        }

        [Fact]
        public void GetAll_ReturnsAllEntities()
        {
            SeedEntities();

            var result = _testRepository.GetAll();

            Assert.NotNull(result);
            Assert.NotEmpty(result);
            Assert.Equal(4, result.Count());
        }

        private void SeedEntities()
        {
            _testContext.TestEntities.AddRange(
                new List<TestEntity>
                {
                    new TestEntity
                    {
                        Id = 1,
                        Value = "one"
                    },
                    new TestEntity
                    {
                        Id = 2,
                        Value = "two"
                    },
                    new TestEntity
                    {
                        Id = 3,
                        Value = "three"
                    },
                    new TestEntity
                    {
                        Id = 4,
                        Value = "four"
                    },
                });

            _testContext.SaveChanges();
        }
    }
}
