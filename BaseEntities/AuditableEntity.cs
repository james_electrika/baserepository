﻿using BaseRepository.Interfaces;
using System;

namespace BaseEntities
{
    public abstract class AuditableEntity<TValue> : KeyedEntity<TValue>, IAuditableEntity
    {
        public DateTime DateCreated { get; set; }

        public DateTime? DateModified { get; set; }

        public DateTime? DateDeleted { get; set; }

        public bool Deleted { get; set; }

        public void Delete()
        {
            Deleted = true;
            DateDeleted = DateTime.UtcNow;
            DateModified = DateTime.UtcNow;
        }
    }
}
