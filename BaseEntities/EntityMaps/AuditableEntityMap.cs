﻿using BaseEntities.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BaseEntities.EntityMaps
{
    public class AuditableEntityMap<TEntity, TId> : KeyedEntityMap<TEntity, TId>
        where TEntity : AuditableEntity<TId>
        where TId : struct
    {
        protected IEntityMapConfiguration Config { get; }

        public AuditableEntityMap(IEntityMapConfiguration config)
        {
            Config = config;
        }

        public override void Configure(EntityTypeBuilder<TEntity> builder)
        {
            base.Configure(builder);

            builder
                .Property(t => t.DateCreated)
                .HasColumnName("DateCreated")
                .HasDefaultValueSql(Config.IsSqlServer ? "GETUTCDATE()" : "CURRENT_TIMESTAMP");

            builder
                .Property(t => t.DateModified)
                .HasColumnName("DateModified")
                .IsRequired(false);

            builder
                .Property(t => t.DateDeleted)
                .HasColumnName("DateDeleted")
                .IsRequired(false);

            builder
                .Property(t => t.Deleted)
                .HasColumnName("Deleted")
                .HasDefaultValue(false);
        }
    }
}
