﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BaseEntities.EntityMaps
{
    public class KeyedEntityMap<TEntity, TValue> : IEntityTypeConfiguration<TEntity>
        where TEntity : KeyedEntity<TValue>
        where TValue : struct
    {
        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder.HasKey(t => t.Id);

            builder
                .Property(t => t.Id)
                .HasColumnName("id")
                .ValueGeneratedOnAdd();
        }
    }
}
