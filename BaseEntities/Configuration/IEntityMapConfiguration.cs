﻿namespace BaseEntities.Configuration
{
    public interface IEntityMapConfiguration
    {
        bool IsSqlServer { get; }
    }
}
