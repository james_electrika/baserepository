﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BaseRepository.Interfaces;
using BaseRepository.Models;
using Microsoft.EntityFrameworkCore;

namespace BaseRepository
{
    /// <summary>
    /// Base Repository compatible with entity framework core and micro ORMs like Dapper
    /// Designed to do complex read queries with Dapper and write operations with EF Core
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TId"></typeparam>
    public class Repository<TEntity, TId> : ReadOnlyRepository, IRepository<TEntity, TId>
        where TEntity : class
    {
        protected readonly DbContext Context;
        protected readonly DbSet<TEntity> Entities;

        protected Repository(DbContext context, IDbConnectionFactory connectionFactory) : base(connectionFactory)
        {
            Context = context;
            Entities = context.Set<TEntity>();
        }

        /// <summary>
        /// Basic get query using entity id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Entity of the defined type</returns>
        public virtual TEntity Get(TId id)
        {
            return Entities.Find(id);
        }

        /// <summary>
        /// Basic get query using entity id async
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Entity of the defined type</returns>
        public virtual async Task<TEntity> GetAsync(TId id)
        {
            return await Entities.FindAsync(id);
        }

        /// <summary>
        /// Return all Entities
        /// </summary>
        /// <returns>List of Entities</returns>
        public virtual IQueryable<TEntity> GetAll()
        {
            return Entities;
        }

        /// <summary>
        /// Return all Entities async
        /// </summary>
        /// <returns>List of Entities</returns>
        public virtual async Task<IQueryable<TEntity>> GetAllAsync()
        {
            return await Task.FromResult(Entities);
        }

        /// <summary>
        /// Return all Entities Within the given page query.
        /// </summary>
        /// <returns>List of Entities</returns>
        public virtual IPagedResult<TEntity> GetPagedResults(IPageQuery pageQuery, Expression<Func<TEntity, bool>> predicate = null)
        {
            return PaginateToPagedResult(Entities, pageQuery, predicate);
        }

        /// <summary>
        /// Return all Entities Within the given page query async.
        /// </summary>
        /// <returns>List of Entities</returns>
        public virtual async Task<IPagedResult<TEntity>> GetPagedResultsAsync(IPageQuery pageQuery, Expression<Func<TEntity, bool>> predicate = null)
        {
            return await PaginateToPagedResultAsync(Entities, pageQuery, predicate);
        }

        /// <summary>
        /// Paginates an IQueryable entity collection with option for orderBy predicate.
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="pageQuery"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        protected static IPagedResult<TEntity> PaginateToPagedResult(IQueryable<TEntity> entities, IPageQuery pageQuery, Expression<Func<TEntity, bool>> predicate = null)
        {
            var result = GetPagedResult(entities, pageQuery);
            var results = ApplyPagination(entities, pageQuery, predicate);

            result.Results = results.ToList();

            return result;
        }

        /// <summary>
        /// Paginates an IQueryable entity collection with option for orderBy predicate.
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="pageQuery"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        protected static async Task<IPagedResult<TEntity>> PaginateToPagedResultAsync(IQueryable<TEntity> entities, IPageQuery pageQuery, Expression<Func<TEntity, bool>> predicate = null)
        {
            var result = GetPagedResult(entities, pageQuery);
            var results = ApplyPagination(entities, pageQuery, predicate);

            result.Results = await results.ToListAsync();

            return result;
        }

        /// <summary>
        /// Paginates an IQueryable entity collection with option for orderBy predicate.
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="pageQuery"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        protected static IQueryable<TEntity> PaginateResults(IQueryable<TEntity> entities, IPageQuery pageQuery, Expression<Func<TEntity, bool>> predicate = null)
        {
            return ApplyPagination(entities, pageQuery, predicate);
        }

        private static IPagedResult<TEntity> GetPagedResult(IQueryable<TEntity> entities, IPageQuery pagequery)
        {
            return new PagedResult<TEntity>
            {
                PageCount = (entities.Count() + pagequery.PageSize - 1) / pagequery.PageSize,
                CurrentPage = pagequery.Page,
                PageSize = pagequery.PageSize,
                RowCount = entities.Count()
            };
        }

        private static IQueryable<TEntity> ApplyPagination(IQueryable<TEntity> entities, IPageQuery pageQuery, Expression<Func<TEntity, bool>> predicate = null)
        {
            var results = entities
                .Skip(pageQuery.Page * pageQuery.PageSize)
                .Take(pageQuery.PageSize);

            if (predicate != null)
                results = results.OrderBy(predicate);

            return results;
        }
    }
}
