﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BaseRepository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BaseRepository
{
    public class AuditableRepository<TEntity, TId> : Repository<TEntity, TId> where TEntity : class, IAuditableEntity
    {
        protected AuditableRepository(DbContext context, IDbConnectionFactory connectionFactory) : base(context, connectionFactory)
        {
        }

        /// <summary>
        /// Basic get query using entity id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Entity of the defined type</returns>
        public override TEntity Get(TId id)
        {
            return Entities.Find(id);
        }

        /// <summary>
        /// Basic get query using entity id async
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Entity of the defined type</returns>
        public override async Task<TEntity> GetAsync(TId id)
        {
            return await Entities.FindAsync(id);
        }

        /// <summary>
        /// Return all Entities
        /// </summary>
        /// <returns>List of Entities</returns>
        public override IQueryable<TEntity> GetAll()
        {
            return Entities.Where(entity => !entity.Deleted);
        }

        /// <summary>
        /// Return all Entities async
        /// </summary>
        /// <returns>List of Entities</returns>
        public override async Task<IQueryable<TEntity>> GetAllAsync()
        {
            return await Task.FromResult(Entities.Where(entity => !entity.Deleted));
        }

        /// <summary>
        /// Return all Entities Within the given page query.
        /// </summary>
        /// <returns>List of Entities</returns>
        public override IPagedResult<TEntity> GetPagedResults(IPageQuery pageQuery, Expression<Func<TEntity, bool>> predicate = null)
        {
            var entities = Entities.Where(entity => !entity.Deleted);

            return PaginateToPagedResult(entities, pageQuery, predicate);
        }

        /// <summary>
        /// Return all Entities Within the given page query async.
        /// </summary>
        /// <returns>List of Entities</returns>
        public override async Task<IPagedResult<TEntity>> GetPagedResultsAsync(IPageQuery pageQuery, Expression<Func<TEntity, bool>> predicate = null)
        {
            var entities = Entities.Where(entity => !entity.Deleted);

            return await PaginateToPagedResultAsync(entities, pageQuery, predicate);
        }
    }
}
