﻿using System.Data;
using System.Data.SqlClient;
using BaseRepository.Interfaces;

namespace BaseRepository
{
    /// <summary>
    /// Class to create and open sql connections, mainly used for repositories that depend on Dapper
    /// </summary>
    public class DbConnectionFactory : IDbConnectionFactory
    {
        private readonly string _connectionString;

        public DbConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Starts and open a database connection to the sql database
        /// </summary>
        /// <returns>Database connection to use with dapper</returns>
        public IDbConnection StartConnection()
        {
            var connection = new SqlConnection(_connectionString);
            connection.Open();
            return connection;
        }
    }
}
