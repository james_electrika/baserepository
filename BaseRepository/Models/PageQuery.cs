﻿using BaseRepository.Interfaces;

namespace BaseRepository.Models
{
    public class PageQuery : IPageQuery
    {
        public int Page { get; set; }

        public int PageSize { get; set; }

        public string Query { get; set; }

        public static PageQuery Default => new PageQuery
        {
            PageSize = 30,
            Page = 0
        };
    }
}
