﻿using System.Collections.Generic;
using System.Data;
using BaseRepository.Interfaces;
using Dapper;

namespace BaseRepository.Models
{
    /// <summary>
    /// Base class to generate a list parameter that can be passed to an sql stored procedure or sql function
    /// Before being able to use it you must create a custom type in your database. Ex:
    /// CREATE TYPE [dbo].[TList] AS TABLE
    /// (
    ///   Value T
    /// )
    /// Where TList is the type name and T is the data type that will hold
    /// </summary>
    /// <typeparam name="T">Data type that is accepted by sql</typeparam>
    public class ListTableParameter<T> : IListTableParameter
    {
        public DataTable DataTable { get; set; }
        private readonly string _parameterName;

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="values">List of values to be inserted</param>
        /// <param name="name">name of the corresponding sql data type in the database</param>
        public ListTableParameter(IEnumerable<T> values, string name)
        {
            _parameterName = name;
            DataTable = new DataTable();
            DataTable.Columns.Add("Value", typeof(T));
            foreach (var value in values)
            {
                DataTable.Rows.Add(value);
            }
        }

        /// <summary>
        /// Creates the sql parameter to be inserted into your ORM to call the stored proc or sql function
        /// </summary>
        /// <returns>Sql parameter</returns>
        public SqlMapper.ICustomQueryParameter ToSqlParameter()
        {
            return DataTable.AsTableValuedParameter(_parameterName);
        }
    }
}
