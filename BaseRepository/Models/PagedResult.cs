﻿using System.Collections.Generic;
using BaseRepository.Interfaces;

namespace BaseRepository.Models
{
    public class PagedResult<T> : IPagedResult<T>
    {
        public IList<T> Results { get; set; }

        public int CurrentPage { get; set; }

        public int PageCount { get; set; }

        public int PageSize { get; set; }

        public int RowCount { get; set; }
    }
}
