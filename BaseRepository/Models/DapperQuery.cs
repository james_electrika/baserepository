﻿namespace BaseRepository.Models
{
    /// <summary>
    /// Convenience class to hold variables for a dapper query
    /// </summary>
    public class DapperQuery
    {
        public object QueryParameters;
        public string SqlQuery;
    }
}
