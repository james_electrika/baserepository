# Base Repository

This is a .net core library that serves as foundation to implement a data layer following the repository and unit of work patterns.

## Dependencies

* .Net Core App

* Entity FrameWork Core

* Dapper

## Important Concepts

### Repository: 

A repository is a class that handles interaction with the database.

Typically you would have at least one repository per database table in SQL and would define CRUD operations there so it can serve as an abstraction layer between the database and the rest of the application

### Unit of Work:

The unit of work is a class that contains all the repositories and handles transactions.

The main responsibility of the Unit of Work is instantiate all the repositories and complete the database changes

# How to Implement

To implement a repository pattern you need a DbContext (from entity framework core) and a Unit of work.

You can get your db context by setting up your models project with [Entity Framework core](https://docs.microsoft.com/en-us/ef/core/get-started/) 

To set your unit of work first, start by creating an interface (for Dependency injection purposes) that inherits from IUnitOfWork.

You should define here (using interfaces) the repositories that your unit of work contain.

``` C#
    public interface IMyUnitOfWork : IUnitOfWork
    {
        IMyFirstRepo FirstRepo { get }
    }
```

In the project that you will be injecting this you should never reference the unit of work implementation, just the interface.

Once you have the interface, just define a new Unit of work as follows

``` C#
    public class MyUnitOfWork : UnitOfWork<MyDbContext>, IMyUnitOfWork
    {
        public IMyFirstRepo FirstRepo { get }

        public MyUnitOfWork(MyDbContext context, IDbConnectionFactory connectionFactory) : base(context, connectionFactory) 
        {
            //Instantiate here your repositories
        }
    }
```

By default, the "Complete" method implements the save changes method from entity framework, you can override the method if you need to.

The Connection factory is a simple class that takes a connection string and can open sql connections, is used to make queries with dapper. You can make a custom connection factory by implementing the interface

## Injecting the unit of work

Once you have defined the unit of work you can inject it in your projects. 

If you are using the repository in another .net core library just reference the interface, let the main .net core application that you are building inject the unit of work dependency and pass the connection strings.

Is recommended to create a configuration class in your repository project and add an extension method to make the injection cleaner

``` C#
    public static class MyRepositoryConfigurations
    {
        public static void AddMyRepository(this IServiceCollection services, string connectionString)
        {
            services.AddScoped<IDbConnectionFactory>(s =>
                new DbConnectionFactory(connectionString));
            services.AddScoped<IJotUnitOfWork, MyUnitOfWork>();
        }
    }
```

Adding as scoped is a good idea so the data context is only shared within the request

Now in your startup class:

``` C#
    public void ConfigureServices(IServiceCollection services)
    {
        //Configure your ef core db context first
        services.AddDbContext<JotContext>(options =>
            {
                //Picking up the connection string from the json configuration file
                options.UseSqlServer(Configuration.GetConnectionString("MyConnectionString"));
            });

        //Then inject your repository
        services.AddJotRepository(Configuration.GetConnectionString("MyConnectionString"));
    }
```

That's it, now you just need some repositories

Since the unit of work takes care of instantiating the repositories you don't need to include them in your startup class. Just inject the unit of work

## Adding Repositories

Depending if you need access to entity framework and dapper or just dapper you will have to choose between Repository and ReadOnlyRepository respectively. 

Let's see how to add a regular repository

First define an interface that inherits from IRepository and define the Entity that will map to.


``` C#
    public interface IMyFirstRepo : IRepository<MyEntity>
    {
        //Define methods here
    }
```

Then define a repository class that inherits from Repository and implements the previously created interface

The base repository has the basic get method pre-defined you can make your own generic repository that inherits from Repository and define other generic methods that are common across your repositories.

``` C#
    public class MyFirstRepo : Repository<MyEntity>, IMyFirstRepo
    {
        public MyFirstRepo(MyDbContext context, IDbConnectionFactory connectionFactory) : base(context, connectionFactory)
        {
            
        }

        //define methods here
    }
```

Don't forget to add your repository to your unit of work

# Examples

``` C#
//Unit of work interface
    public interface IJotUnitOfWork : IUnitOfWork
    {
        INodesRepository Nodes { get;  }
        INodeChildrenRepository NodeChildren { get; }
    }

//Unit of work implementation
    public class JotUnitOfWork : UnitOfWork<JotContext>, IJotUnitOfWork
    {

        public INodesRepository Nodes { get; }
        public INodeChildrenRepository NodeChildren { get; }

        public JotUnitOfWork(JotContext context, IDbConnectionFactory connectionFactory) : base(context, connectionFactory)
        {
            //EF Core + Dapper repo
            Nodes = new NodesRepository(_context, _connectionFactory);

            //Dapper Only Repo
            NodeChildren = new NodeChildrenRepository(_connectionFactory);
        }
    }

//Nodes Repository interface
    public interface INodesRepository : IRepository<Node>
    {
        List<Node> GetPartNodesFromBrand(int id, int? skip, int? take);
        Task<List<Node>> GetNodeChildren(int startNodeId);
        Task<List<Node>> GetNodeParents(int endNodeId);
    }

//Nodes Repository Implementation. Ef Core + Dapper
    public class NodesRepository : Repository<Node>, INodesRepository
    {
        public NodesRepository(DbContext context, IDbConnectionFactory connectionFactory) : base(context, connectionFactory)
        {
            
        }

        //Ef Core Query

        public List<Node> GetPartNodesFromBrand(int brandId, int? skip, int? take)
        {
            var list = Entities.Where(node => IsNodePartFromBrand(node, brandId))
                .OrderBy(e => e.Id)
                .Skip(skip ?? 0)
                .Take(take ?? 10000)
                .ToList();

            return list;
        }

        //Dapper query using stored procedures
        public async Task<List<Node>> GetNodeChildren(int startNodeId)
        {
            var query = new DapperQuery()
            {
                SqlQuery = "EXEC dbo.Sproc_GetNodeChildren @StartNodeId = @Id",
                QueryParameters = new
                {
                    Id = startNodeId
                }
            };

            return await ExecuteNodeQuery(query);
        }

        //Dapper query using stored procedures
        public async Task<List<Node>> GetNodeParents(int endNodeId)
        {
            var query = new DapperQuery()
            {
                SqlQuery = "EXEC dbo.Sproc_getNodeParents @EndNodeId = @Id",
                QueryParameters = new
                {
                    Id = endNodeId
                }
            };

            return await ExecuteNodeQuery(query);
        }

        //Calling the db query using dapper
        private async Task<List<Node>> ExecuteNodeQuery(DapperQuery query)
        {
            using (IDbConnection connection = _connectionFactory.StartConnection())
            {
                var result = await connection.QueryAsync<Node>(query.SqlQuery, query.QueryParameters);
                return result.ToList();
            }
        }

        private bool IsNodePartFromBrand(Node node, int brandId)
        {
            return node.BrandNode == brandId && node.NodeType == NodeType.Part;
        }

    }

//Node Children Interface
    public interface INodeChildrenRepository
    {
        Task<List<NodeWithChildrenAndAttributes>> Get(int startNodeId, int startLevel, List<String> titleList);

        Task<List<NodeWithChildrenAndAttributes>> GetWithinLevels(int startNodeId, int startLevel, int levelsDeep,
            List<string> titleList);
    }

//Node Children Implementation: Dapper Only

    public class NodeChildrenRepository : ReadOnlyRepository, INodeChildrenRepository
    {
        public NodeChildrenRepository(IDbConnectionFactory connectionFactory) : base(connectionFactory)
        {
        }

        public async Task<List<NodeWithChildrenAndAttributes>> Get(int startNodeId, int startLevel, List<string> titleList)
        {
                var query = new DapperQuery()
                {
                    SqlQuery =
                        "Exec dbo.Sproc_GetNodeChildrenWithAttributes @StartNodeId = @nodeId, @Level = @sLevel, @titleList = @list",
                    QueryParameters = new
                    {
                        nodeId = startNodeId,
                        sLevel = startLevel,
                        list = new StringListTableParameter(titleList).GetSqlParameter()
                    }
                };

                return await ExecuteGetQuery(startLevel, query);
        }

        public async Task<List<NodeWithChildrenAndAttributes>> GetWithinLevels(int startNodeId, int startLevel, int levelsDeep, List<string> titleList)
        {
            if (levelsDeep <= 0)
            {
                return new List<NodeWithChildrenAndAttributes>();
            }
            var query = new DapperQuery()
            {
                SqlQuery =
                    "Exec dbo.Sproc_GetAllNodeChildrenWithAttributesWithinLevels @StartNode = @StartId, @StartLevel = @Level, @LevelsDeep = @Depth, @Attributes = @AttrList",
                QueryParameters = new
                {
                    StartId = startNodeId,
                    Level = startLevel,
                    Depth = levelsDeep,
                    AttrList = new StringListTableParameter(titleList).GetSqlParameter()
                }
            };

            return await ExecuteGetWithinLevelsQuery(startLevel, query);
        }

        private async Task<List<NodeWithChildrenAndAttributes>> ExecuteGetQuery(int startLevel, DapperQuery query)
        {
            using (IDbConnection connection = _connectionFactory.StartConnection())
            {
                var result = new List<NodeWithChildrenAndAttributes>();
                NodeWithChildrenAndAttributes lastNode = null;
                await connection
                    .QueryAsync<NodeWithChildrenAndAttributes, SortableNodeAttribute, NodeWithChildrenAndAttributes>(query.SqlQuery,
                        (node, attribute) =>
                        {
                            if (!IsSameNode(lastNode, node))
                            {
                                   if (lastNode == null || node.Level <= startLevel + 1)
                                    {
                                        result.Add(node);
                                    }
                                    else if (IsNodeChildren(lastNode, node))
                                    {
                                        lastNode.Children.Add(node);
                                    }
                                    else
                                    {
                                        var parent = result.FirstOrDefault(n => n.Id == node.ParentId);
                                        parent.Children.Add(node);
                                    }

                                lastNode = node;
                            }

                            if (attribute != null)
                            {
                                lastNode.Attributes.Add(attribute);
                            }


                            return null;
                        }, query.QueryParameters);

                return result;
            }
        }

        //Dapper Query with custom mapping
        private async Task<List<NodeWithChildrenAndAttributes>> ExecuteGetWithinLevelsQuery(int startLevel,
            DapperQuery query)
        {
            using (IDbConnection connection = _connectionFactory.StartConnection())
            {
                var result = new List<NodeWithChildrenAndAttributes>();
                NodeWithChildrenAndAttributes lastNode = null;
                var nodePath = new List<NodeWithChildrenAndAttributes>();
                await connection.QueryAsync<NodeWithChildrenAndAttributes, SortableNodeAttribute, NodeWithChildrenAndAttributes>(query.SqlQuery,
                    (node, attribute) =>
                    {
                        if (!IsSameNode(lastNode, node))
                        {
                            if (lastNode == null || node.Level <= startLevel + 1)
                            {
                                result.Add(node);
                                nodePath = new List<NodeWithChildrenAndAttributes>()
                                {
                                    node
                                };
                            }
                            else
                            {
                                var parent = FindParent(nodePath, node);
                                nodePath.Add(node);
                                parent.Children.Add(node);
                            }
                            lastNode = node;
                        }
                        if (attribute != null)
                        {
                            lastNode.Attributes.Add(attribute);
                        }
                        return null;
                    },
                    query.QueryParameters);
                return result;
            }
        }

        private NodeWithChildrenAndAttributes FindParent(List<NodeWithChildrenAndAttributes> nodePath, NodeWithChildrenAndAttributes node)
        {
            return nodePath.Find(n => n.Id == node.ParentId);
        }

        private bool IsSameNode(NodeWithChildrenAndAttributes lastNode, NodeWithChildrenAndAttributes newNode)
        {
            if (lastNode == null)
            {
                return false;
            }

            return lastNode.Id == newNode.Id;
        }

        private bool IsNodeChildren(NodeWithChildrenAndAttributes lastNode, NodeWithChildrenAndAttributes newNode)
        {
            return lastNode.Id == newNode.ParentId;
        }

    }

//Configurations class for dependency injection
    public static class JotRepositoryConfigurations
    {
        public static void AddJotRepository(this IServiceCollection services, string connectionString)
        {
            services.AddScoped<IDbConnectionFactory>(s =>
                new DbConnectionFactory(connectionString));
            services.AddScoped<IJotUnitOfWork, JotUnitOfWork>();
        }
    }

```

# Other Utilities

## List Table Parameters

A list table parameter is simply a way to pass an array as a parameter for a stored procedure or an sql function

The simplest way to use it would be something like this:

Define an sql type

``` SQL
    CREATE TYPE [dbo].[TList] AS TABLE
     (
       Value T
     )
```

Where TList is the type name and T is the data type that will hold.

Then just instantiate a new ListTableParameter with the sql parameter name and the collection of items

``` C#
    var list = new List<T>() { T1, T2...}
    var sqlParameter = new ListTableParameter(list, "TList").GetSqlParameter()

```

A better approach would be to create a new class that inherits from ListTableParameter defining the type and the sql parameter name

Example:

Sql type
``` SQL
    CREATE TYPE [dbo].[IntArray] AS TABLE
    (
	    Value INT
    )
```

table parameter
``` C#
    public class IntArrayTableParameter : ListTableParameter<int>
    {
        public IntArrayTableParameter(IEnumerable<int> values) : base(values, "IntArray")
        {

        }

    }

```

Usage:

``` C#
        public async Task<List<NodeAndAttributes>> GetFromNodeIdList(IEnumerable<int> idList)
        {
            var query = new DapperQuery()
                {
                    SqlQuery = "EXEC dbo.Sproc_GetNodeAndAllAttributesFromNodeIdList @NodeIdList = @IdList",
                    QueryParameters = new
                    {
                        IdList = new IntArrayTableParameter(idList).GetSqlParameter()
                    }
                };
            return await ExecuteGetFromNodeQuery(query);
        }

        private async Task<List<NodeAndAttributes>> ExecuteGetFromNodeQuery(DapperQuery query)
        {
            using (IDbConnection connection = _connectionFactory.StartConnection())
            {
                var result = new List<NodeAndAttributes>();
                NodeAndAttributes lastNode = null;
                await connection.QueryAsync<Node, NodeAttribute, Node>(query.SqlQuery,
                    (node, attr) =>
                    {
                        if (!IsSameNode(lastNode, node))
                        {
                            lastNode = new NodeAndAttributes()
                            {
                                Node = node
                            };
                            result.Add(lastNode);
                        }

                        lastNode.NodeAttributes.Add(attr);

                        return null;
                    }, query.QueryParameters);

                return result;
            }
        }
```