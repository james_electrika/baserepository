﻿namespace BaseRepository.Interfaces
{
    public interface IPageQuery
    {
        int Page { get; }

        int PageSize { get; }

        string Query { get; }
    }
}
