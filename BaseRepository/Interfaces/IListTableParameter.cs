﻿using System.Data;
using Dapper;

namespace BaseRepository.Interfaces
{
    public interface IListTableParameter
    {
        DataTable DataTable { get; set; }
        SqlMapper.ICustomQueryParameter ToSqlParameter();
    }
}
