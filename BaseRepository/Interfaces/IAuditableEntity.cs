﻿using System;

namespace BaseRepository.Interfaces
{
    public interface IAuditableEntity
    {
        DateTime DateCreated { get; set; }

        DateTime? DateModified { get; set; }

        DateTime? DateDeleted { get; set; }

        bool Deleted { get; set; }
    }
}
