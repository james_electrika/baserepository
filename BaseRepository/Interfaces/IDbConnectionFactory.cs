﻿using System.Data;

namespace BaseRepository.Interfaces
{
    public interface IDbConnectionFactory
    {
        IDbConnection StartConnection();
    }
}
