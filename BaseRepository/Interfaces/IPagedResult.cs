﻿using System.Collections.Generic;

namespace BaseRepository.Interfaces
{
    public interface IPagedResult<T>
    {
        int CurrentPage { get; set; }

        int PageCount { get; set; }

        int PageSize { get; set; }

        int RowCount { get; set; }

        IList<T> Results { get; set; }
    }
}
